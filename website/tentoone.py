from flask import Blueprint, redirect, render_template, request, flash, jsonify, session
from flask_login import login_required, current_user
from sqlalchemy import asc, desc, select
from .models import TenToOne, Point
from . import db
import json
from datetime import date

tentoone = Blueprint("tentoone", __name__)


@tentoone.route("/", methods=["GET", "POST"])
@login_required
def home():
    tentoone = TenToOne.query.all()

    if "ordering" in request.args:
        if request.args["ordering"] == "point":
            tentoone = TenToOne.query.join(Point).order_by(asc(Point.point)).all()
        if request.args["ordering"] == "-point":
            tentoone = TenToOne.query.join(Point).order_by(desc(Point.point)).all()
        if request.args["ordering"] == "date":
            tentoone = TenToOne.query.order_by(asc(TenToOne.date)).all()
        if request.args["ordering"] == "-date":
            tentoone = TenToOne.query.order_by(desc(TenToOne.date)).all()
        if request.args["ordering"] == "state":
            tentoone = TenToOne.query.join(Point).order_by(asc(Point.position)).all()
        if request.args["ordering"] == "-state":
            tentoone = TenToOne.query.join(Point).order_by(desc(Point.position)).all()

    return render_template(
        "tentoone/home.html",
        title="Jeden z dziesięciu",
        user=current_user,
        query=tentoone,
    )


@tentoone.route("/add-data", methods=["GET", "POST"])
@login_required
def add_data():
    if request.method == "POST":
        num_episode = request.form.get("num_episode")
        num_series = request.form.get("num_series")
        episode_date = request.form.get("date")
        string_date = episode_date.split("-")
        date_obj = date(int(string_date[0]), int(string_date[1]), int(string_date[2]))
        link = request.form.get("link")
        name = request.form.get("name")
        point = request.form.get("point")
        position = request.form.get("position")

        new_data = TenToOne(
            num_episode=int(num_episode),
            num_series=int(num_series),
            date=date_obj,
            link=link,
        )
        new_point = Point(
            name=name, point=int(point), position=position, episode=new_data
        )
        db.session.add(new_data)
        db.session.add(new_point)
        db.session.commit()
        flash("Dodano dane!", category="success")
        return redirect("/tentoone")

    return render_template(
        "tentoone/add-data.html", title="Dodaj dane", user=current_user
    )


@tentoone.route("/edit-data/<int:id>", methods=["GET", "POST"])
@login_required
def edit_data(id):
    data = TenToOne.query.get_or_404(id)

    if request.method == "POST":
        data.num_episode = request.form.get("num_episode")
        data.num_series = request.form.get("num_series")
        date_old = request.form.get("date")
        string_date = date_old.split("-")
        data.date = date(int(string_date[0]), int(string_date[1]), int(string_date[2]))
        data.link = request.form.get("link")
        data.point.name = request.form.get("name")
        data.point.point = request.form.get("point")
        data.point.position = request.form.get("position")

        try:
            db.session.commit()
            flash("Aktualizowano dane!", category="success")
            return redirect("/tentoone")
        except:
            return "Problem"

    return render_template(
        "tentoone/edit-data.html", title="Dodaj dane", user=current_user, data=data
    )


@tentoone.route("/delete-data/<int:id>", methods=["GET", "POST"])
@login_required
def delete_data(id):
    data = TenToOne.query.get_or_404(id)

    if request.method == "POST":
        try:
            db.session.delete(data)
            db.session.commit()
            flash("Usunięto dane!", category="success")
            return redirect("/tentoone")
        except:
            return "Problem"

    return render_template(
        "tentoone/delete-data.html", title="Usuń dane", user=current_user, data=data
    )
