from . import db
from flask_login import UserMixin
from sqlalchemy.sql import func
from datetime import date
from .enums import Position


class Note(db.Model):
    __tablename__ = "note"
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String(10000))
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))


class User(db.Model, UserMixin):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True)
    password = db.Column(db.String(150))
    first_name = db.Column(db.String(150))
    notes = db.relationship("Note")


class TenToOne(db.Model):
    __tablename__ = "tentoone"
    id = db.Column(db.Integer, primary_key=True)
    num_episode = db.Column(db.Integer)
    num_series = db.Column(db.Integer)
    date = db.Column(db.Date(), default=date.today())
    link = db.Column(db.VARCHAR(), unique=True)
    point = db.relationship("Point", backref="episode", uselist=False)


class Point(db.Model):
    __tablename__ = "point"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150))
    point = db.Column(db.Integer)
    position = db.Column(db.Enum(Position), server_default="0")
    episode_id = db.Column(db.Integer, db.ForeignKey("tentoone.id"))

