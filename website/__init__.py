from os import path

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path
from flask_login import LoginManager
from flask_migrate import Migrate

db = SQLAlchemy()
app = Flask(__name__)
app.config["SECRET_KEY"] = "key"
migrate = Migrate(app, db)
DB_NAME = "database.db"


def create_app():

    from .views import views
    from .auth import auth
    from .tentoone import tentoone

    app.register_blueprint(views, url_prefix="/note")
    app.register_blueprint(auth, url_prefix="/auth")
    app.register_blueprint(tentoone, url_prefix="/tentoone")
    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{DB_NAME}"

    from .models import Note, TenToOne, User

    db.init_app(app)
    create_database(app)
    login_manager = LoginManager()
    login_manager.login_view = "auth.login"
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))

    return app


def create_database(app):
    if not path.exists("website/" + DB_NAME):
        db.create_all(app=app)
        print("Created Database!")
