FROM python:3.9-alpine

WORKDIR /app

COPY . /app

RUN pip --no-cache-dir install -r requirements.txt 

ENTRYPOINT  ["python"]

CMD ["main.py"]